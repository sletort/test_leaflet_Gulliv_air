var express = require('express');
var app = express();

function getCoords()
{
	// get a number between 48 and 49
	lon = Math.random() + 48

	// get a number between -2 and -1
	lat = Math.random() - 2

	return [ lon, lat ]
}

// get coords WS
app.get('/coords', function (req, res)
	{

		json = [ getCoords() ]
		console.log( json )
		res.end( JSON.stringify( json ) )
	})

var server = app.listen(8081, function ()
	{
		var host = server.address().address
		var port = server.address().port

		console.log("Example app listening at http://%s:%s", host, port )
	})
