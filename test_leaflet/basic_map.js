/*
 * L is defined by leaflet
*/

// define a leaflet map
var mymap = L.map('map').setView([48.1, -1.6], 12);

// get tile layer from OSM
var tile_url = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
// locally after download with marble-qt
// var tile_url = 'file:///home/kook/.local/share/marble/maps/earth/openstreetmap/{z}/{x}/{y}.png'
L.tileLayer( tile_url, {
		attribution: 'OSM, KooK',
		maxZoom: 18,
		id: 'mapbox.streets',
	}).addTo(mymap);

